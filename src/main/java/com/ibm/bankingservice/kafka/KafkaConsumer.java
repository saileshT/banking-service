package com.ibm.bankingservice.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.bankingservice.infra.LogMethodParam;
import com.ibm.bankingservice.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaConsumer{

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);
	
	@Autowired
	TransactionService transactionService;
	
	@LogMethodParam
	@KafkaListener(topics = "${transactions.topic}", groupId = "summary")
	public void listen(ConsumerRecord<String, TransactionEvent> record) throws Exception {
		LOGGER.info("received payload='{}' to topic='{}'", record.value(), record.topic());
		transactionService.createTransaction(new ObjectMapper().readValue(String.valueOf(record.value()), TransactionEvent.class));
	}
}
