package com.ibm.bankingservice.repository;

import com.ibm.bankingservice.domain.Transaction;
import com.ibm.bankingservice.enums.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import javax.persistence.LockModeType;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction,String> {
    List<Transaction> findByAccountNumberAndTransactionDateBetween(String accountNumber, Instant fromDate, Instant toDate);
    List<Transaction> findByAccountNumberAndTransactionTypeAndTransactionDateBetween(String accountNumber, TransactionType type, Instant fromDate, Instant toDate);
    List<Transaction> findByAccountNumber(String accountNumber);
    List<Transaction> findByAccountNumberAndTransactionType(String accountNumber, TransactionType type);
    
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Transaction> findFirstByAccountNumberOrderByTransactionDateDesc(String accountNumber);
}
