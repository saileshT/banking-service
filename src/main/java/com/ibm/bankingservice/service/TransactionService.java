package com.ibm.bankingservice.service;

import com.ibm.bankingservice.dto.TransactionDTO;
import com.ibm.bankingservice.enums.TransactionType;
import com.ibm.bankingservice.kafka.TransactionEvent;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface TransactionService {
    Optional<List<TransactionDTO>> getSummary(String accountNumber, Instant fromDate, Instant toDate);

	Optional<List<TransactionDTO>> getSummary(String accountNumber, TransactionType type, Instant fromDate, Instant toDate);

	Optional<Double> getBalance(String accountNumber);

	void createTransaction(TransactionEvent record);

	void loadData(TransactionEvent transactionEvent);
}
