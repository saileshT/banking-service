package com.ibm.bankingservice.service.impl;

import com.ibm.bankingservice.domain.Transaction;
import com.ibm.bankingservice.dto.TransactionDTO;
import com.ibm.bankingservice.enums.TransactionType;
import com.ibm.bankingservice.exception.BusinessException;
import com.ibm.bankingservice.infra.LogMethodParam;
import com.ibm.bankingservice.kafka.KafkaProducer;
import com.ibm.bankingservice.kafka.TransactionEvent;
import com.ibm.bankingservice.repository.TransactionRepository;
import com.ibm.bankingservice.service.TransactionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionServiceImpl.class);

	@Autowired
	private KafkaProducer kafkaProducer;

    @Autowired
    private TransactionRepository transactionRepository;

	@Override
	@LogMethodParam
	public Optional<Double> getBalance(String accountNumber){
	Optional<Transaction> transaction = transactionRepository.findFirstByAccountNumberOrderByTransactionDateDesc(accountNumber);
	if(!transaction.isPresent()) {
		return Optional.empty();
	}
		return Optional.of(transaction.get().getBalance());
	}

	@Override
	@LogMethodParam
	public void createTransaction(TransactionEvent transactionEvent) {
		Optional<Transaction> OptTransaction = transactionRepository.findFirstByAccountNumberOrderByTransactionDateDesc(transactionEvent.getAccountNumber());
		double balance = 0;
		if(!OptTransaction.isPresent()) {
		    if(transactionEvent.getTransactionType().equals(TransactionType.withdraw)) {
			    throw new BusinessException("You can't withdraw before depositing, We are doing a business here.. not any charity :P ");
		    } else{
				balance = transactionEvent.getAmount();
		    }
		 } else {
		 if (transactionEvent.getTransactionType().equals(TransactionType.deposit)) {
			 balance = OptTransaction.get().getBalance() + transactionEvent.getAmount();
		 } else {
		    balance = OptTransaction.get().getBalance() - transactionEvent.getAmount();
		 }
		}
	
		Transaction transaction = Transaction.builder().accountNumber(transactionEvent.getAccountNumber())
			.transactionDate(transactionEvent.getTransactionDate())
			.amount(transactionEvent.getAmount())
			.transactionType(transactionEvent.getTransactionType())
			.balance(balance).build();
			
		transactionRepository.save(transaction);
	}

	@Override
	@LogMethodParam
	public void loadData(TransactionEvent transactionEvent) {
		if(transactionEvent == null || transactionEvent.getAmount() == null || transactionEvent.getAmount() < 0.0 ||
				StringUtils.isBlank(transactionEvent.getAccountNumber()) ){
			throw new IllegalArgumentException("Provide valid input for transaction event");
		}
		transactionEvent.setTransactionDate(Instant.now());
		kafkaProducer.send(transactionEvent);
	}

	@Override
	@LogMethodParam
    public Optional<List<TransactionDTO>> getSummary(String accountNumber, Instant fromDate, Instant toDate) {
        if(fromDate == null || toDate == null){
        	return Optional.of(buildTransactions(transactionRepository.findByAccountNumber(accountNumber)));
        }else{
        	return Optional.of(buildTransactions(transactionRepository.findByAccountNumberAndTransactionDateBetween(accountNumber, fromDate, toDate)));
        }
    }
    
    @Override
    @LogMethodParam
    public Optional<List<TransactionDTO>> getSummary(String accountNumber, TransactionType type, Instant fromDate, Instant toDate){
        if(fromDate == null || toDate == null){
        	return Optional.of(buildTransactions(transactionRepository.findByAccountNumberAndTransactionType(accountNumber,type)));
        }else{
	        return Optional.of(buildTransactions(transactionRepository.findByAccountNumberAndTransactionTypeAndTransactionDateBetween(accountNumber,type, fromDate, toDate)));
        }
    }

    private List<TransactionDTO> buildTransactions(List<Transaction> transactions) {
        return transactions.stream().map(this::buildTransaction).collect(Collectors.toList());
    }

    private TransactionDTO buildTransaction(Transaction transaction) {
        return TransactionDTO.builder()
                .transactionId(transaction.getTransactionId())
                .accountNumber(transaction.getAccountNumber())
                .transactionDate(transaction.getTransactionDate())
                .transactionType(transaction.getTransactionType())
                .amount(transaction.getAmount())
                .balance(transaction.getBalance())
                .build();
    }
}
